<?php

namespace UEdit\Controller;

use Silex\ControllerProviderInterface;
use Silex\Application;

class DataProviderController implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];

        $controllers->get('/formats', function () use ($app) {
            $response = array(
                "msg" => "",
                "formats" => $app['dataprovider']->getAvailableFormats()
            );
            return $app->json($response);
        });

        $controllers->get('/cts', function () use ($app) {
            $response = array(
                "msg" => "",
                "cts" => $app['dataprovider']->getAvailableContentTypes()
            );

            return $app->json($response);
        });

        $controllers->get('/blockelements', function () use ($app) {
            $response = array(
                "msg" => "",
                "elements" => $app['dataprovider']->getAvailableBlockElements()
            );

            return $app->json($response);
        });

        $controllers->get('/editorialelements/{ct}/{format}', function ($ct, $format) use ($app) {

            $validCts = array("noticia","album","autocover","external");
            $validFormats = array("web","mobile","tablet");
            if (!in_array($ct, $validCts) || !in_array($format, $validFormats)) {
                return $app->json(array('msg' => 'El formato y/o el ct son incorrectos.'), 400);
            }

            $editorialelementsAutocover = array(
                array(
                    'name' => 'titulo',
                    'required' => true,
                    'multi' => false,
                    'fields' => array(),
                ),
                array(
                    'name' => 'apoyo',
                    'required' => false,
                    'multi' => false,
                    'fields' => array(),
                ),
                array(
                    'name' => 'hueco publicidad',
                    'required' => false,
                    'multi' => true,
                    'fields' => array('type'),
                )
            );

            $editorialelementsNoticia = array();

            $response = array(
                "msg" => "",
                "elements" => $editorialelementsAutocover
            );

            return $app->json($response);
        });

        return $controllers;
    }
}
