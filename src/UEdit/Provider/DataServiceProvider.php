<?php

namespace UEdit\Provider;

use Silex\ServiceProviderInterface;
use Silex\Application;

class DataServiceProvider implements ServiceProviderInterface
{

    public function register(Application $app)
    {
        $app['dataprovider'] = function () {
            return new DataProvider();
        };
    }

    public function boot(Application $app)
    {

    }
}
