<?php

namespace UEdit\Provider;

class DataProvider
{

    public function getAvailableContentTypes()
    {
        $cts =array(
            array("prettyName"=> "Noticia", "value"=> "noticia"),
            array("prettyName"=> "Album", "value"=> "album"),
            array("prettyName"=> "Autocover", "value"=> "autocover"),
            array("prettyName"=> "Otros contenidos", "value"=> "external"),
        );

        return $cts;
    }

    public function getAvailableFormats()
    {
        return array("web","mobile","tablet");
    }

    public function getAvailableBlockElements()
    {
        return array(
            'aside',
            'article',
            'footer',
            'header',
            'contenedor',
            'contenido',
        );
    }

    public function getAvailableGroups()
    {
        return array(
            'section-h1',
            'section-h2',
            'div-h1',
            'div-h2',
        );
    }

    public function getAvailableEditorialElements($contentType, $format)
    {
        if (!$this->isValidContentType($contentType)
            || !in_array($format, $this->getAvailableFormats())) {
            return null;
        }

        $editorialelements = array(
            array(
                'name' => 'titulo',
                'required' => true,
                'multi' => false,
                'properties' => array(),
            ),
            array(
                'name' => 'antetitulo',
                'required' => false,
                'multi' => false,
                'properties' => array(),
            ),
            array(
                'name' => 'subtitulo',
                'required' => false,
                'multi' => false,
                'properties' => array(),
            ),
            array(
                'name' => 'texto',
                'required' => false,
                'multi' => false,
                'properties' => array(),
            ),
            array(
                'name' => 'numcomentarios',
                'required' => false,
                'multi' => true,
                'properties' => array(),
            ),
            array(
                'name' => 'comentarios',
                'required' => false,
                'multi' => false,
                'properties' => array(),
            ),
            array(
                'name' => 'herramientas',
                'required' => false,
                'multi' => false,
                'properties' => array(),
            ),
            array(
                'name' => 'redes-sociales',
                'required' => false,
                'multi' => false,
                'properties' => array(),
            ),
            array(
                'name' => 'tags',
                'required' => false,
                'multi' => false,
                'properties' => array(),
            ),
            array(
                'name' => 'valoraciones',
                'required' => false,
                'multi' => false,
                'properties' => array(),
            ),
            array(
                'name' => 'hueco multimedia',
                'required' => false,
                'multi' => true,
                'properties' => array('width', 'height', 'name'),
            )
        );

        return $editorialelements;
    }

    private function isValidContentType($contentType)
    {
        foreach ($this->getAvailableContentTypes() as $ct) {
            if ($ct['value'] == $contentType) {
                return true;
            }
        }
        return false;
    }
}
