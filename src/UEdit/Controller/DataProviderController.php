<?php

namespace UEdit\Controller;

use Silex\ControllerProviderInterface;
use Silex\Application;

use Symfony\Component\HttpFoundation\Request;


class DataProviderController implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];

        $controllers->get('/formats', function () use ($app) {
            $response = array(
                "msg" => "",
                "formats" => $app['dataprovider']->getAvailableFormats(),
                "default" => 'web'
            );
            return $app->json($response);
        });

        $controllers->get('/cts', function () use ($app) {
            $response = array(
                "msg" => "",
                "cts" => $app['dataprovider']->getAvailableContentTypes()
            );

            return $app->json($response);
        });

        $controllers->get('/groups', function () use ($app) {
            $response = array(
                "msg" => "Agrupaciones disponibles.",
                "groups" => $app['dataprovider']->getAvailableGroups()
            );

            return $app->json($response);
        });

        $controllers->get('/blockelements', function () use ($app) {
            $response = array(
                "msg" => "",
                "elements" => $app['dataprovider']->getAvailableBlockElements()
            );

            return $app->json($response);
        });

        $controllers->get('/editorialelements/{ct}/{format}', function (Request $request, $ct, $format) use ($app) {

            $version = $request->get('version', 'v1');//Mocked the latest version to v4.3

            $editorialElements = $app['dataprovider']->getAvailableEditorialElements($ct, $format);
            if (null == $editorialElements) {
                return $app->json(array('msg' => 'El formato y/o el ct son incorrectos.'), 400);
            }

            $response = array(
                "msg" => "",
                "elements" => $editorialElements,
                "version" => $version,
            );

            return $app->json($response);
        });

        return $controllers;
    }
}
